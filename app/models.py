from app import Model
from app.utils import ModelActions
from sqlalchemy import TEXT, Column, DateTime, Enum, Integer
from sqlalchemy.sql.functions import now


class USER(Model, ModelActions):
    __tablename__ = 'user'

    STATUS_NEW = 'NEW'
    STATUS_RECREATED = 'recreated'

    id = Column(Integer, primary_key=True, autoincrement=True)

    created_at = Column(DateTime, nullable=False, default=now())
    name = Column(TEXT, nullable=False)
    country = Column(TEXT)
    status = Column(Enum(STATUS_NEW, STATUS_RECREATED), default=STATUS_NEW,
                       nullable=False)
