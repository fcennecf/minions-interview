import logging
import sys
from uuid import uuid4


from flask import Flask
from flask import g
from flask import request

from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base

import app.config as default_config


def __setup_logging(app_inst):
    file_handler = logging.StreamHandler(sys.stdout)
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
    app_inst.logger.addHandler(file_handler)
    if app_inst.config['DEBUG']:
        app_inst.logger.setLevel(logging.DEBUG)
    else:
        app_inst.logger.setLevel(logging.INFO)


def __setup_config(app_inst):
    # @todo env-s
    app_inst.config.from_object(default_config)


def log_app(msg, *args):
    app.logger.info("%%s %s" % msg, uuid4(), *args)


def get_user_ip() -> str:
    ip = None
    if not request.headers.get("X-Forwarded-For"):
        ip = request.remote_addr
    return ip


app = Flask(__name__)
__setup_config(app)
__setup_logging(app)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
Model = declarative_base()

from app.views import api, user
