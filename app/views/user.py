from app import app, log_app
from app.models import USER
from flask import Response, jsonify


@app.route('/health-status')
def health():
    try:
        USER.query().filter_by(user_id=0).count()
        db_check = True
    except Exception as e:
        log_app('exception %s' % e)
        db_check = False
    return jsonify({'db': db_check})
