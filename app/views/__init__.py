import binascii
import os
import time
from typing import Optional

from flask import request, jsonify, g

from app import log_app, app, get_user_ip

TOKEN_HEADER_NAME = 'Auth-Token'


def token_auth() -> Optional[dict]:
    token = request.headers.get(TOKEN_HEADER_NAME)
    log_app('check token %s', token)

    token_config = app.config['TOKENS'].get(token)
    log_app('token config %s', token_config)

    return token_config


@app.before_request
def before_request():
    g.request_uid = binascii.b2a_hex(os.urandom(20))
    g.request_start_time = time.time()
    log_app("request %s %s %s", request.url, request.method, get_user_ip())


@app.after_request
def after_request(response):
    response_time = time.time() - g.request_start_time
    log_app("response %s %s", response.status_code, response_time)
    log_app("splunk response-time-stat time=%s,code=%s,path=%s,ip=%s", response_time, response.status_code,
            request.path, get_user_ip())
    return response
