import requests
from flask import abort, request
from flask import jsonify

from app import app, log_app
from app import db
from app import get_user_ip
from app.models import USER
from app.views import token_auth


def get_country_by_ip(ip):
    country_by_ip = None
    try:
        log_app('Call external service')
        result = requests.get(f'http://finder-country/ip/{ip}').json()
        return result['country']
    except Exception:
        pass
    return country_by_ip


@app.route('/api/user/new', methods=["GET"])
def new_user():
    token_config = token_auth()
    if not token_config:
        abort(403)

    user_id = request.args.get('user_id', None)
    user_gender = request.args.get('user_gender')
    user_name = request.args.get('user_name', None)

    if len(user_name) > 10:
        return {}, 404

    user_data = {
        'user_id': user_id,
        'user_name': user_name,
        'user_gender': user_gender,
        'country': get_country_by_ip(get_user_ip())
    }

    if not user_id:
        user_data['status'] = 'NEW'
        customer_obj = USER(**user_data)
        db.session.add(customer_obj)
        db.session.commit()
    else:
        user_data['status'] = 'RECREATED'
        USER.get_by_id(user_id).remove()
        USER(**user_data).create()

    return jsonify({
        'user_name': user_name
    })
