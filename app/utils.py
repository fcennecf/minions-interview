from app import db
from sqlalchemy.orm import Query


class ModelActions(object):
    @classmethod
    def query(cls) -> Query:
        return cls.session().query(cls)

    @classmethod
    def session(cls):
        return db.session

    @classmethod
    def get_by_id(cls, find_id):
        q = cls.query().filter_by(id=find_id)
        return q.limit(1).first()

    def create(self):
        self.session().add(self)
        self.session().commit()
        return self

    def remove(self) -> None:
        self.session().delete(self)
        self.session().commit()

    def update(self, data: dict, id_field: str = 'id') -> None:
        id_value = int(getattr(self, id_field, 0))
        self.query().filter_by(**{id_field: id_value}).update(data, synchronize_session=False)
        self.session().commit()
