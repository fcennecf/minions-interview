import os

import dotenv

dotenv.load_dotenv(dotenv.find_dotenv())

IS_STAGE = os.getenv('IS_STAGE', default='')

SQLALCHEMY_DATABASE_URI = os.getenv('PGSQL_URI')
SQLALCHEMY_TRACK_MODIFICATIONS = False

APP_TOKEN = os.getenv('APP_TOKEN')

TOKENS = {
    APP_TOKEN: {'name': 'SECRET_TOKEN'},
}
